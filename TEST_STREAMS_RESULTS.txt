CON 100 CARACTERES

SendServerDefProm=166,64
ReceptClientDefProm=295,09
sendClientDefProm=81,73
ReceivServerDefProm=80,82
(Descartado)
-----------------------------

SendServerDataProm=71,09
ReceptClientDataProm=131,36
sendClientDataProm=1552,82
ReceivServerDataProm=57,00
-----------------------------
SendServerBuffIOProm=116,00
ReceptClientBuffIOProm=125,55
sendClientBuffIOProm=6533,82
ReceivServerBuffIOProm=83,09
-----------------------------
SendServerBTProm=112,45
ReceptClientBTProm=233,64
sendClientBTProm=136,36
ReceivServerBTProm=145,00
-----------------------------
SendServerSRProm=195,82
ReceptClientSRProm=286,00
sendClientSRProm=84,09
ReceivServerSRProm=382,45
-----------------------------

(Solo se tomara en consideracion los streams de bytes)
----------------------------------------------------------
Con 100.000 Caracteres

SendServerDefProm=5223,75
ReceptClientDefProm=10329,75  (Ganador)
sendClientDefProm=2797,08
ReceivServerDefProm=15820,42  (Ganador)
-----------------------------
SendServerDataProm=5571,00
ReceptClientDataProm=29300,33
sendClientDataProm=866,08  (Ganador)
ReceivServerDataProm=25058,50
-----------------------------
SendServerBuffIOProm=725,67 (Ganador)
ReceptClientBuffIOProm=16612,75
sendClientBuffIOProm=4667,58
ReceivServerBuffIOProm=18360,83
-----------------------------


----------------------------------------------------------
Con 1048576 caracteres (1 MB)
SendServerDefProm=19914,17 
ReceptClientDefProm=57724,13
sendClientDefProm=10634,50
ReceivServerDefProm=20812,73
-----------------------------
SendServerDataProm=3798,20 (Ganador)
ReceptClientDataProm=55025,00 (Ganador)
sendClientDataProm=5407,97 (Ganador)
ReceivServerDataProm=18820,27
-----------------------------
SendServerBuffIOProm=7177,03
ReceptClientBuffIOProm=57483,90
sendClientBuffIOProm=12577,63
ReceivServerBuffIOProm=15196,63 (Ganador)
-----------------------------

----------------------------------------------------------
Con 2097152 caracteres (2 MB)

SendServerDefProm=28713,64
ReceptClientDefProm=67414,73
sendClientDefProm=30592,82
ReceivServerDefProm=41461,36
-----------------------------
SendServerDataProm=7154,64  (Ganador)
ReceptClientDataProm=61384,91 (Ganador)
sendClientDataProm=12443,73  (Ganador)
ReceivServerDataProm=28059,18  (Ganador)
-----------------------------
SendServerBuffIOProm=9030,18
ReceptClientBuffIOProm=77382,73
sendClientBuffIOProm=18734,91
ReceivServerBuffIOProm=35492,27
-----------------------------

GANADOR FINAL DataStream
