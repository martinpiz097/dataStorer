/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.interfaces;

import org.datastor.cmd.Command;
@FunctionalInterface
/**
 *
 * @author martin
 */
public interface InterpreterListener {
    //public boolean isInterpretable(Command cmd);
    public void exec(Command cmd);
}
