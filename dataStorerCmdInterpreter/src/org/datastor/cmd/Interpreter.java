/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.cmd;

import org.datastor.interfaces.InterpreterListener;

/**
 *
 * @author martin
 */
public class Interpreter {
    private InterpreterListener interListener;
    private String responseMsg;
    
    public Interpreter() {}

    public Interpreter(InterpreterListener interListener) {
        this.interListener = interListener;
    }

//    public boolean isInterpretable(Command cmd){
//        return interListener.isInterpretable(cmd);
//    }
    
    public boolean hasResponse(){
        return responseMsg != null;
    }
    
    public boolean cmdEndsWith(String strCmd, char c){
        return strCmd.length() > 1 && strCmd.charAt(strCmd.length()-1) == c;
    }
    
    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public void clearResponseMsg() {
        responseMsg = null;
    }
    
    // Desde el servidor deben haber reglas ya definidas para las interpretaciones y asi no
    // crearlas de nuevo
    public void execCommand(Command cmd){
        interListener.exec(cmd);
    }
    
    public InterpreterListener getInterListener() {
        return interListener;
    }

    public void setListener(InterpreterListener interListener) {
        this.interListener = interListener;
    }
    
}
