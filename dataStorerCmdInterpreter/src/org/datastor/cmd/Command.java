/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.cmd;

import java.util.ArrayList;
import org.datastor.system.NumberID;

/**
 *
 * @author martin
 */
public class Command implements Interpretable{
    private String order;
    private String[] options;

    public Command(String order, String... options) {
        this.order = order;
        this.options = options;
    }

    public Command(String strCmd) throws BadSyntaxException {
        //countComillas(strCmd);
        buildCommand(strCmd);
        return;
    }
    
//    private void getCommand(String strCmd){
//        StringBuilder sbCmd = new StringBuilder();
//        ArrayList<String> listOptions = new ArrayList<>();
//        final char[] chars = strCmd.toCharArray();
//        final int charsLen  = chars.length;
//        
//        char c;
//        int nextIndex = 0;
//        
//        for (int i = 0; i < charsLen && (c = chars[i]) != ' '; i++, ++nextIndex)
//            sbCmd.append(c);
//        
//        order = sbCmd.toString();
//        sbCmd.delete(0, sbCmd.length());
//        boolean hasOptions = nextIndex < charsLen;
//        if (!hasOptions)
//            return;
//            
//        for (int i = nextIndex; i < charsLen; i++) {
//            
//        }
//        
//    }
    
    private void countComillas(String strCommand) throws BadSyntaxException{
        int count = 0;
        final char[] chars = strCommand.toCharArray();
        for (int i = 0; i < chars.length; i++)
            if (chars[i] == '\'')
               count++;
        if (count > 0 && count % 2 != 0)
            throw new BadSyntaxException("Sintaxis del comando invalida");
    }
    
    // Se esta programando el tema de las comillas simples en el comando
    private void buildCommand(String strCommand) throws BadSyntaxException{
        final ArrayList<String> listOptions = new ArrayList<>();
        final StringBuilder sbCurrent = new StringBuilder();
        final String[] spaceSplit = strCommand.split(" ");
        order = spaceSplit[0];
        if (order.contains("'"))
            throw new BadSyntaxException("La orden no puede contener comillas");
            
        //final char[] chars = strCommand.toCharArray();
        String split;
        boolean addingPhrase = false;
        final int spaceSplitLen = spaceSplit.length;

        for (int i = 1; i < spaceSplitLen; i++) {
            split = spaceSplit[i];
            if (addingPhrase) {
                if (!split.contains("'")){
                    sbCurrent.append(split);
                    if (i < spaceSplitLen-1)
                        sbCurrent.append(' ');
                }
                else if (split.endsWith("'")) {
                    sbCurrent.append(split);
                    listOptions.add(sbCurrent.toString());
                    sbCurrent.delete(0, sbCurrent.length());
                    addingPhrase = false;
                }
                else
                    throw new BadSyntaxException("Error de sintaxis en la opcion "+split);
            }
            // Ver en el interprete cuando un numero es invalido al contener letras
            // ya que en ese caso agregar como string solamente
            else{
                if (split.startsWith("'") && !split.endsWith("'")) {
                    addingPhrase = true;
                    sbCurrent.append(split).append(' ');
                }
                /*else if (!split.startsWith("'") && split.endsWith("'")) {
                    listOptions.add(split.substring(0, split.length()-1));
                    sbCurrent.append(split.charAt(split.length()-1)).append(' ');
                    addingPhrase = true;
                }*/
                else
                    listOptions.add(split);
            }
        }
        // Si falto una comilla al final se añade todo como si esta existiese
        // (Tolerancia al olvido xd)
        if (addingPhrase)
            listOptions.add(sbCurrent.append('\'').toString());

        options = new String[listOptions.size()];
        options = (String[]) listOptions.toArray(options);
    }
    
    public boolean hasOptions(){
        return options != null;
    }
    
    public boolean isNumberOption(int index){
        try {
            Long.parseLong(getOptionAt(index));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean hasNumberID(String option){
        final char lastC = option.charAt(option.length()-1);
        return option.length() > 1 && 
                (lastC == NumberID.LONG || 
                lastC == NumberID.INT || 
                lastC == NumberID.SHORT || 
                lastC == NumberID.BYTE || 
                lastC == NumberID.FLOAT || 
                lastC == NumberID.DOUBLE);
    }
    
    public int getOptionAsNumber(int index){
        return isNumberOption(index) ? Integer.parseInt(getOptionAt(index)) : -1;
    }
    
    public int getOptionsCount(){
        return hasOptions() ? options.length : 0;
    }
    
    public String getOrder() {
        return order;
    }

    public String[] getOptions() {
        return options;
    }
    
    public String getOptionAt(int index){
        return options[index];
    }
    
    @Override
    public String toString(){
        StringBuilder sbCmd = new StringBuilder();
        sbCmd.append(order);
        
        final int optLen = options != null ? options.length : 0;
        for (int i = 0; i < optLen; i++)
            sbCmd.append(' ').append(options[i]);
        return sbCmd.toString();
    }
    
    /*public static void main(String[] args) throws BadSyntaxException {
        Command cmd = new Command("order 'opcion1 opcion2 opcion3 opcion4' opcion5");
        System.out.println(cmd.toString());
        System.out.println("Cantidad de opciones: "+cmd.getOptionsCount());
        for (int i = 0; i < cmd.getOptionsCount(); i++) {
            System.out.printf("Opcion %d: %s\n", i, cmd.getOptionAt(i));
        }
        
    }*/
}
