/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.system;

import java.io.File;

/**
 *
 * @author martin
 */
public class SysInfo {
    public static final short DEFAULT_PORT_1 = 4200;
    public static final short DEFAULT_PORT_2 = 4300;
    public static final short DEFAULT_PORT_3 = 8400;
    public static final String DEFAULT_HOST = "192.168.1.3";
    public static final String LOCALHOST = "localhost";
    public static final String ELEMENTS_SEPARATOR = "°°";
    public static final String OS_NAME = System.getProperty("os.name");
    public static final String USER_NAME = System.getProperty("user.name");
    public static final String DEFUALT_PATH = new StringBuilder().append("/home/")
            .append(USER_NAME).append('/').append("DataStorer/").toString();
    public static final File DEFAULT_ROOT_DIR = new File(DEFUALT_PATH);
    
    // Se usara mas adelante cuando este todo listo
    public static final File DEFAULT_USERS_DIR = new File(DEFUALT_PATH, "users");
    public static final String DEFAULT_KEY = "super";
    
    public static final String CONFIG_NAME_FILE = "config.xml";
    
    static {
        if (!DEFAULT_ROOT_DIR.exists())
            DEFAULT_ROOT_DIR.mkdir();
        if (!DEFAULT_USERS_DIR.exists())
            DEFAULT_USERS_DIR.mkdir();
    }
    
}
