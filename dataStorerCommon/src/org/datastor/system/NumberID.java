/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.system;

/**
 *
 * @author martin
 */
public class NumberID {
    public static final char LONG = 'L';
    public static final char INT = 'I';
    public static final char SHORT = 'S';
    public static final char BYTE = 'B';
    public static final char FLOAT = 'F';
    public static final char DOUBLE = 'D';
}
