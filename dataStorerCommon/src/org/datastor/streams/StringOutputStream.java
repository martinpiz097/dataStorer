/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.streams;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.dsEncryptor.encryptor.Encryptor;
import org.dsEncryptor.exceptions.InvalidTextException;

/**
 *
 * @author martin
 */
public class StringOutputStream extends DataOutputStream{

    private final Encryptor encryptor;
    
    public StringOutputStream(OutputStream out) {
        super(out);
        this.encryptor = new Encryptor();
    }

    public void writeString(final String str) throws IOException, InvalidTextException{
        final byte[] bytes = encryptor.encrypt(str)/*str*/.getBytes();
        super.write(bytes, 0, bytes.length);
        /*final char[] chars = encryptor.encrypt(str).toCharArray();
        byte[] bytes = new byte[chars.length];
        for (int i = 0; i < chars.length; i++)
            bytes[i] = (byte)chars[i];
        super.write(bytes);*/
    }
    
}
