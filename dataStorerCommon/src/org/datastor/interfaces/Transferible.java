/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.interfaces;

/**
 *
 * @author martin
 */
@FunctionalInterface

public interface Transferible {
    public void sendData(String data);
}
