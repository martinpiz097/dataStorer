/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dataStorClient.net;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.datastor.interfaces.Communicable;
import org.datastor.interfaces.Transferible;
import org.datastor.streams.StringOutputStream;
import org.datastor.system.ServerMessages;
import org.datastor.system.SysInfo;
import org.dsEncryptor.exceptions.InvalidTextException;

/**
 *
 * @author martin
 */
public class Sender implements Transferible{
    private final StringOutputStream outputStream;

    public Sender(OutputStream sockStream) {
        outputStream = new StringOutputStream(sockStream);
    }

    @Override
    public void sendData(String data) {
        try {
            outputStream.writeString(data+ServerMessages.REQUEST_MSG);
        } catch (IOException | InvalidTextException ex) {
            Logger.getLogger(Sender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
