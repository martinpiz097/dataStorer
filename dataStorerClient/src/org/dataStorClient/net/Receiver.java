/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dataStorClient.net;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dataStorClient.system.ConsoleColor;
import org.datastor.interfaces.Receivable;
import org.datastor.streams.StringInputStream;
import org.datastor.system.ServerMessages;
import org.datastor.system.SysInfo;
import org.dsEncryptor.exceptions.InvalidTextException;

/**
 *
 * @author martin
 */
public class Receiver implements Receivable{
    private final StringInputStream inputStream;
    private final StringBuilder sbData;
    
    public Receiver(InputStream sockStream) {
        inputStream = new StringInputStream(sockStream);
        sbData = new StringBuilder();
    }
    
    private String getColorByHeader(String header){
        switch(header){
            case ServerMessages.BAD_RANGE:
                return ConsoleColor.ANSI_YELLOW;
            case ServerMessages.ERROR_LOGIN:
                return ConsoleColor.ANSI_RED;
            case ServerMessages.ERROR_OPTION:
                return ConsoleColor.ANSI_RED;
            case ServerMessages.ERROR_ORDER:
                return ConsoleColor.ANSI_RED;
            case ServerMessages.ERROR_REGISTER:
                return ConsoleColor.ANSI_RED;
            case ServerMessages.ERROR_SYNTAX:
                return ConsoleColor.ANSI_CYAN;
            case ServerMessages.GENERAL_ERROR:
                return ConsoleColor.ANSI_RED;
            case ServerMessages.GET_MSG:
                return ConsoleColor.ANSI_WHITE;
            case ServerMessages.HELP_MSG:
                return ConsoleColor.ANSI_WHITE;
            case ServerMessages.OK_MSG:
                return ConsoleColor.ANSI_GREEN;
            case ServerMessages.WARNING_ERROR:
                return ConsoleColor.ANSI_YELLOW;
            default:
                return ConsoleColor.ANSI_RESET;
        }
    }
    
    private String getHeader(String msg){
        return msg.split(ServerMessages.HEADER_SEPARATOR+"")[0];
    }
    
    private String getGETMsg(String originalMsg){
        return originalMsg.replace(SysInfo.ELEMENTS_SEPARATOR, "\n");
    }
    
    private String getFormattedMsg(String msg){
        String outHeader = msg.split(ServerMessages.HEADER_SEPARATOR+"")[1];
        return outHeader.substring(0, outHeader.length()-4);
    }

    @Override
    public String getReceivData() {
        try {
            final String msg = inputStream.readString();
            final String header = getHeader(msg);
            final String color = getColorByHeader(header);
            String finalMsg = getFormattedMsg(msg);
            sbData.append(color).append(finalMsg).append(ConsoleColor.ANSI_RESET);
            finalMsg = sbData.toString();
            sbData.delete(0, sbData.length());
            return finalMsg;
        } catch (IOException | InvalidTextException ex) {
            Logger.getLogger(Receiver.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
