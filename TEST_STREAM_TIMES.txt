Envio de cadena de texto de 100 caracteres
Medicion de tiempo en microsegundos
para el envio de cadenas de texto
-----------------------------------

Streams default:

	Send Server: 187, 183, 195 --> 188,3
	Receiv Client: 658, 574, 677 --> 636,3

	Send Client: 67, 39, 61 --> 55,6
	Receiv Server: 658, 561, 618 --> 612,3
	
	(En comparacion con dataStream solo se salva por SendClient pero queda descartado)

Data Streams:

	Send Server: 51, 158, 53 --> 87
	Receiv Client: 540, 520, 555 --> 538,3

	Send Client: 153, 128, 121 --> 134
	Receiv Server: 542, 551, 524 --> 373,6

----------------------------------------------------------------------------------
DataStream vs BufferedStream

Data Streams:

	Send Server: 157, 166, 162, 176 --> 165,25
	Receiv Client: 587, 688, 687, 562 --> 631

	Send Client: 95, 123, 121, 75 --> 103,5
	Receiv Server: 585, 759, 727, 647 --> 679,5
	(Descartado DataStream)

Buffered Streams:

	Send Server: 75, 62, 61, 114 --> 78
	Receiv Client: 294, 337, 365, 451 --> 361,75

	Send Client: 45, 61, 61, 59 --> 56,5
	Receiv Server:343, 371, 454, 425 --> 398,25

----------------------------------------------------------------------------------
BufferedStream vs BufferedReader/Writer

Buffered Streams:
	Send Server: 134, 136, 146 --> 138,6
	Receiv Client: 195, 320, 265 --> 260

	Send Client: 71, 50, 78 --> 66,3
	Receiv Server: 94, 101, 77 --> 366  


Buffered Reader/Writer
	Send Server: 108, 117, 147 --> 124
	Receiv Client: 197, 190, 203 --> 196,6

	Send Client: 60, 64, 66 --> 63,3
	Receiv Server: 115, 107, 128 --> 116,6
