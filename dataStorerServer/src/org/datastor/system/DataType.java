/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.system;

/**
 *
 * @author martin
 */
public class DataType {
    public static final String STR = "str";
    public static final String LONG = "long";
    public static final String INT = "int";
    public static final String SHORT = "short";
    public static final String BYTE = "byte";
    public static final String FLOAT = "float";
    public static final String DOUBLE = "double";
}
