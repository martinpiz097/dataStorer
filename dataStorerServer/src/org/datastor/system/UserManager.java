/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.system;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.datastor.config.ConfigManager;
import org.datastor.model.User;
import org.datastor.net.Server;
import org.dsEncryptor.encryptor.Encryptor;
import org.martin.electroList.structure.ElectroList;

/**
 *
 * @author martin
 */
public class UserManager {
    private final User user;
    private final File folderUser;
    private final ElectroList<UserList> listArchives;
    private final ConfigManager cnfManager;

    public static enum OPEN_LIST_OPTION{
        CREATE, OPEN_ONLY;
    }
    
    public static boolean create(User user){
        File userFolder = new File(SysInfo.DEFAULT_USERS_DIR, user.getNick());
        if (userFolder.exists())
            return false;
        userFolder.mkdir();
        Encryptor srvEncryptor = Server.SYS_ENCRYPTOR;
        // Se creo el archivo de configuraciones
        ConfigManager cnfManager = new ConfigManager(new File(userFolder, SysInfo.CONFIG_NAME_FILE));
        cnfManager.setConfigValue("nick", user.getNick());
        cnfManager.setConfigValue("passw", user.getPassword());
        return true;
    }
    
    public UserManager(User user, boolean create){
        folderUser = new File(SysInfo.DEFAULT_USERS_DIR, user.getNick());
        if (create)
            folderUser.mkdir();
        else if (!folderUser.exists())
            throwFNFException("La carpeta usuario de nombre "+
                    folderUser.getName()+" no existe");
        this.user = user;
        listArchives = new ElectroList<>();
        cnfManager = new ConfigManager(new File(folderUser, SysInfo.CONFIG_NAME_FILE));
        loadLists();
    }
    
    public UserManager(User user, File folderUser) {
        if (!folderUser.exists()) {
            throwFNFException("La carpeta usuario de nombre "+
                    folderUser.getName()+" no existe");
        }
        this.user = user;
        this.folderUser = folderUser;
        listArchives = new ElectroList<>();
        cnfManager = new ConfigManager(new File(folderUser, SysInfo.CONFIG_NAME_FILE));
        loadLists();
    }

    public UserManager(User user) {
        this(user, new File(SysInfo.DEFAULT_USERS_DIR, user.getNick()));
    }
    
    private void throwFNFException(String msg){
        try {
            throw new FileNotFoundException(msg);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadLists(){
        File[] fileLists = folderUser.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return !pathname.isDirectory() && !pathname.getName().endsWith(".xml");
            }
        });
        if (fileLists != null)
            for (int i = 0; i < fileLists.length; i++)
                listArchives.add(new UserList(fileLists[i]));
    }

    public int getListsCount(){
        return listArchives.size();
    }
    
    public File getFolderUser() {
        return folderUser;
    }

    /**
     * Crea una nueva lista para el usuario.
     * @param name Nombre de la lista nueva.
     */
    public void addUserList(String name){
        listArchives.add(new UserList(folderUser, name));
    }
    
    public ElectroList<UserList> getListArchives() {
        return listArchives;
    }

    public ConfigManager getCnfManager() {
        return cnfManager;
    }
    
    public UserList getListBy(String name){
        System.out.println("Cantidad de listas cargadas: "+listArchives.size());
        return listArchives.findFirst(list->list.getName().equals(name));
        //return listArchives.parallelSearch(list->list.getName().equals(name))
        //        .peekFirst();
        /*for (UserList list : listArchives) 
            if (list.getName().equals(name))
                return list;
        return null;*/
    }
    
    public boolean clearAll(){
        if (listArchives.isEmpty())
            return false;
        if(listArchives.allMatch(ul->ul.getListData().isEmpty()))
            return false;
        
        for (UserList ul : listArchives)
            ul.clearAll();
        return true;
    }
    
    
    // Los metodos solo guardan las comprobaciones se hacen afuera
    public void add(String data, String listName, int index){
        UserList list = getListBy(listName);
        if (index == -1)
            list.add(data);
        else
            list.add(index, data);
    }
    
    public void add(String[] data, String listName, int index){
        UserList list = getListBy(listName);
        if (index == -1)
            list.add(data);
        else
            list.add(index, data);
    }
    
    public boolean clearAll(String listName){
        UserList list = getListBy(listName);
        if (list == null)
            return false;
        list.clearAll();
        return true;
    }
    
    public boolean create(String listName){
        UserList newList = getListBy(listName);
        System.out.println(newList);
        if (newList != null)
            return false;
        newList = new UserList(folderUser, listName, OPEN_LIST_OPTION.CREATE);
        listArchives.add(newList);
        return true;
    }
    
    public void deleteAll() {
        for (UserList userList : listArchives)
            userList.delete();
        listArchives.clear();
    }
    
    public void deleteList(String listName) {
        int listIndex = 0;
        for (UserList userList : listArchives) {
            if (userList.getName().equals(listName)) {
                userList.delete();
                break;
            }
            listIndex++;
        }
        listArchives.remove(listIndex);
    }
    
    public boolean deleteInIndex(String listName, int index){
        UserList list = getListBy(listName);
        return list == null ? true : list.deleteInIndex(index);
    }
    
    public boolean deleteIf(String listName, String filter, String symbol){
        UserList list = getListBy(listName);
        if(list == null)
            return false;
        return list.deleteIf(filter, symbol);
    }
    
    public int getSizeOfAll(){
        int sizeOf = 0;
        sizeOf = listArchives.stream()
                .map((list) -> list.getSizeOf()).reduce(sizeOf, Integer::sum);
        return sizeOf;
    }
    
    public int getSizeOf(String listName){
        UserList list = getListBy(listName);
        if(list == null)
            return -1;
        return list.getSizeOf();
    }
    
    public String getAllFrom(String listName){
        UserList list = getListBy(listName);
        if(list == null)
            return ServerMessages.NULL;
        return list.getAll();
    }
    
    public String getByRangeFrom(String listName, int start, int end){
        UserList list = getListBy(listName);
        if(list == null)
            return ServerMessages.NULL;
        if (start > end || (start < 0 || end < 0))
            return ServerMessages.BAD_RANGE;
        
        return list.getByRange(start, end);
    }
    
    public String getByIndexFrom(String listName, int index){
        UserList list = getListBy(listName);
        if(list == null)
            return ServerMessages.NULL;
        return list.getByIndex(index);
    }
    
}
