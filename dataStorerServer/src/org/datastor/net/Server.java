/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.datastor.config.ConfigManager;
import org.datastor.net.thread.TClient;
import org.datastor.net.thread.TRequestManager;
import org.datastor.config.InfoManager;
import org.datastor.model.User;
import org.datastor.streams.StringInputStream;
import org.datastor.streams.StringOutputStream;
import org.datastor.system.SysInfo;
import org.dsEncryptor.encryptor.Encryptor;
import org.martin.electroList.structure.ElectroList;

/**
 *
 * @author martin
 */
public class Server extends Thread{
    private final ServerSocket serverSock;
    private final ElectroList<TClient> listClientsConnected;
    private final ConfigManager configManager;
    private final InfoManager infoManager;
    private static Server thisObj;

    public static final Encryptor SYS_ENCRYPTOR = new Encryptor(SysInfo.DEFAULT_KEY);
    
    public static Server getServer(){
        return thisObj;
    }
    
    public static void newInstance(){
        try {
            thisObj = new Server();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Server() throws IOException{
        serverSock = new ServerSocket(SysInfo.DEFAULT_PORT_1);
        listClientsConnected = new ElectroList<>();
        configManager = new ConfigManager();
        infoManager = new InfoManager();
    }
    
    public ConfigManager getConfigManager() {
        return configManager;
    }

    public InfoManager getInfoManager() {
        return infoManager;
    }
    
    public void addClient(Client client){
        TClient newCli = new TClient(client);
        newCli.start();
        listClientsConnected.add(newCli);
    }
    
    public void addClient(User user, Socket sock, 
            StringOutputStream os, StringInputStream is){
        TClient newCli = new TClient(new Client(user, sock, os, is));
        newCli.start();
        listClientsConnected.add(newCli);
    }
    
    public void disconnectAll(){
        for (TClient client : listClientsConnected)
            client.disconnectClient();
    }
    
    public void disconnectClient(long threadId) {
        int idClient = 0;
        for (TClient tClient : listClientsConnected) {
            if (tClient.getId() == threadId) {
                tClient.closeConnection();
                break;
            }
            idClient++;
        }
        listClientsConnected.remove(idClient);
    }
    
    @Override
    public void run(){
        while (true) {
            try {
                System.out.println("Antes del nuevo cliente!");
                new TRequestManager(serverSock.accept()).start();
                System.out.println("Cliente conectado!");
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void main(String[] args) {
        Server.newInstance();
        Server.getServer().start();
    }

}
