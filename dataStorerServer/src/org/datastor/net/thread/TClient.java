/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.net.thread;

import org.datastor.cmd.BadSyntaxException;
import org.datastor.cmd.Command;
import org.datastor.interfaces.Communicable;
import org.datastor.model.User;
import org.datastor.net.Client;
import org.datastor.net.Server;
import org.datastor.system.ServerMessages;
import org.datastor.system.UserManager;

/**
 *
 * @author martin
 */
public class TClient extends Thread implements Communicable{
    private final Client client;
    private final StringBuilder sbMsg;
    private final ConnectionManager connManager;
    private boolean isConnected;
    
    public TClient(Client client) {
        this.client = client;
        sbMsg = new StringBuilder();
        isConnected = true;
        connManager = new ConnectionManager(client.getRemoteAddress(), getId());
        setName("TClient: "+client.getUser().getNick());
    }
    
    public boolean isConnected(){
        return client.isConnected();
    }
    
    public void closeStreams(){
        client.closeStreams();
    }
    
    public void closeConnection(){
        client.closeStreams();
        client.closeSocket();
    }

    public Client getClient() {
        return client;
    }
    
    public User getUser(){
        return client.getUser();
    }
    
    public UserManager getUserManager(){
        return client.getUserManager();
    }
    
    public void disconnectClient(){
        isConnected = false;
    }
    
    public void execCommand(String strCommand) throws BadSyntaxException{
        client.getInterpreter().execCommand(new Command(strCommand));
    }
    
    public void sendResponseMessage(){
        sendData(client.getInterpreter().getResponseMsg());
    }
    
    public void sendSyntaxErrorMessage(String msg){
        sbMsg.append(ServerMessages.ERROR_SYNTAX).append(ServerMessages.HEADER_SEPARATOR)
                .append(msg).append(ServerMessages.RESPONSE_MSG);
        sendData(sbMsg.toString());
        sbMsg.delete(0, sbMsg.length());
    }
    
    @Override
    public void sendData(String data) {
        client.sendData(data);
    }

    @Override
    public String getReceivData() {
        // Mensaje sin el finalizador
        String msg = client.getReceivData();
        return msg.substring(0, msg.length()-4);
    }
    
    @Override
    public void run(){
        //connManager.start();
        // Recordar que despues los mensajes se enviaran con formato especifico
        String msgRec;
        while (isConnected()) {
            msgRec = getReceivData();
            System.out.println("TClientComandoRecibido: "+msgRec);
            try {
                execCommand(msgRec);
                System.out.println("TClientResponseMessage: "+client
                    .getInterpreter().getResponseMsg());
                sendResponseMessage();
            } catch (BadSyntaxException ex) {
                sendSyntaxErrorMessage(ex.getMessage());
            }
        }
        Server.getServer().disconnectClient(getId());
    }
    
}
