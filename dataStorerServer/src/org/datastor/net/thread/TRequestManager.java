/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.net.thread;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.datastor.cmd.BadSyntaxException;
import org.datastor.cmd.CmdOrder;
import org.datastor.cmd.Command;
import org.datastor.cmd.Interpreter;
import org.datastor.interfaces.Communicable;
import org.datastor.model.User;
import org.datastor.net.Client;
import org.datastor.net.Server;
import org.datastor.streams.StringInputStream;
import org.datastor.streams.StringOutputStream;
import org.datastor.system.ServerMessages;
import org.datastor.system.UserManager;
import org.dsEncryptor.exceptions.InvalidTextException;

/**
 *
 * @author martin
 */
public class TRequestManager extends Thread implements Communicable{
    private final Socket sockRequest;
    private final StringOutputStream outputStream;
    private final StringInputStream inputStream;
    private Interpreter requestInterpreter;
    
    private boolean connected;
    
    //private static final byte EOF = -1;
    
    public TRequestManager(Socket sockRequest) throws IOException {
        this.sockRequest = sockRequest;
        outputStream = new StringOutputStream(sockRequest.getOutputStream());
        inputStream = new StringInputStream(sockRequest.getInputStream());
        instanceInterpreter();
        setName("TRequestManager: "+getId());
    }
    
    private void instanceInterpreter(){
        requestInterpreter = new Interpreter();
        requestInterpreter.setListener((Command cmd) -> {
            final String CMD_ORDER = cmd.getOrder();
            final int optCount = cmd.getOptionsCount();
            StringBuilder sbMsg = new StringBuilder();
            
            switch(CMD_ORDER){
                case CmdOrder.CLOSE:
                     if (optCount == 0) {
                            buildMessage(ServerMessages.OK_MSG, "Conexion cerrada con exito");
                            connected = false;
                     }
                     else
                         buildMessage(ServerMessages.ERROR_OPTION, "Este comando no posee parametros");
                    break;
                
                case CmdOrder.LOGIN:
                    if (cmd.hasOptions()) {
                        if (cmd.getOptionsCount() == 2) {
                            String nick = cmd.getOptionAt(0);
                            String passw = cmd.getOptionAt(1);
                            boolean isValid = Server.getServer().getInfoManager()
                                    .existsUser(nick, passw);
                            if (isValid) {
                                sbMsg.append(ServerMessages.OK_MSG)
                                        .append('-')
                                        .append("Login correcto!")
                                        .append(ServerMessages.RESPONSE_MSG);
                                requestInterpreter.setResponseMsg(sbMsg.toString());
                                
                                // Crear el cliente con los datos del usuario
                                User loged = new User(nick, passw);
                                Client newCli = new Client(loged, sockRequest, outputStream, inputStream);
                                Server.getServer().addClient(newCli);
                            }
                            else{
                                // Mas adelante dejar solo el identificador del error
                                // de login
                                sbMsg.append(ServerMessages.ERROR_LOGIN)
                                        .append('-').append("Error de login")
                                        .append(ServerMessages.RESPONSE_MSG);
                                requestInterpreter.setResponseMsg(sbMsg.toString());
                            }
                        }
                        else{
                            sbMsg.append(ServerMessages.ERROR_OPTION)
                                    .append('-').append("Cantidad de opciones invalida")
                                    .append(ServerMessages.RESPONSE_MSG);
                            requestInterpreter.setResponseMsg(sbMsg.toString());
                        }
                    }
                    else{
                        sbMsg.append(ServerMessages.ERROR_OPTION)
                                .append('-').append("Comando sin opciones")
                                .append(ServerMessages.RESPONSE_MSG);
                        requestInterpreter.setResponseMsg(sbMsg.toString());
                    }
                    break;
                    
                case CmdOrder.REGISTER:
                    if(cmd.getOptionsCount() == 3) {
                        String nick = cmd.getOptionAt(0);
                        String pass1 = cmd.getOptionAt(1);
                        String pass2 = cmd.getOptionAt(2);
                        if (!pass1.equals(pass2)) {
                            sbMsg.append(ServerMessages.ERROR_REGISTER).append('-')
                                    .append("Las contraseñas no coinciden")
                                    .append(ServerMessages.RESPONSE_MSG);
                            requestInterpreter.setResponseMsg(sbMsg.toString());
                        } 
                        else {
                            pass2 = null;
                            User newUser = new User(nick, pass1);
                            boolean created = UserManager.create(newUser);
                            sbMsg.append(created ? ServerMessages.OK_MSG : ServerMessages.ERROR_REGISTER)
                                    .append('-').append(created ? "Usuario creado con exito" : "Nick ya existente")
                                    .append(ServerMessages.RESPONSE_MSG);
                            requestInterpreter.setResponseMsg(sbMsg.toString());
                        }
                    }
                    else {
                        sbMsg.append(ServerMessages.ERROR_OPTION).append('-')
                                .append("Cantidad de opciones invalida")
                                .append(ServerMessages.RESPONSE_MSG);
                        requestInterpreter.setResponseMsg(sbMsg.toString());
                    }
                    
                    break;
                
                default:
                    if (CmdOrder.isAvailableOrder(CMD_ORDER))
                        sbMsg.append(ServerMessages.GENERAL_ERROR)
                                .append('-').append("Comando no disponible para su uso");
                    else
                        sbMsg.append(ServerMessages.ERROR_ORDER)
                                .append('-').append("Comando desconocido");
                    sbMsg.append(ServerMessages.RESPONSE_MSG);
                    requestInterpreter.setResponseMsg(sbMsg.toString());
                    sbMsg = null;
                    break;
            }
        });
    }
    
    private void buildMessage(String header, String msg){
        StringBuilder sbMsg = new StringBuilder();
        sbMsg.append(header).append('-').append(msg).append(ServerMessages.RESPONSE_MSG);
        requestInterpreter.setResponseMsg(sbMsg.toString());
    }
    
    public void execCommand(String strCommand) throws BadSyntaxException{
        requestInterpreter.execCommand(new Command(strCommand));
    }
    
    public void sendResponseMessage(){
        sendData(requestInterpreter.getResponseMsg());
    }
    
    public void sendSyntaxErrorMessage(String msg){
        StringBuilder sbMsg = new StringBuilder();
        sbMsg.append(ServerMessages.ERROR_SYNTAX).append(ServerMessages.HEADER_SEPARATOR)
                .append(msg).append(ServerMessages.RESPONSE_MSG);
        sendData(sbMsg.toString());
        sbMsg.delete(0, sbMsg.length());
    }
    
    @Override
    public void sendData(String data) {
        try {
            outputStream.writeString(data);
        } catch (IOException | InvalidTextException ex) {
            Logger.getLogger(TRequestManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getReceivData() {
        try {
            // Mensaje sin el finalizador
            String msg = inputStream.readString();
            return msg.substring(0, msg.length()-4);
        } catch (IOException | InvalidTextException ex) {
            Logger.getLogger(TRequestManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    
    @Override
    public void run(){
        try {
            // Recordar que despues los mensajes se enviaran con formato especifico
            String msg = getReceivData();
            System.out.println("Dato recibido: "+msg);
            // Hacer un encryptor igual en el cliente
            execCommand(msg);
            System.out.println("Respuesta a enviar: "+requestInterpreter.getResponseMsg());
            sendResponseMessage();
            System.out.println("Se envio respuesta!");
        } catch (BadSyntaxException ex) {
            sendSyntaxErrorMessage(ex.getMessage());
        }
    }

}
