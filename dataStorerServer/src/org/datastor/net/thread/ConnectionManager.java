/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.net.thread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.datastor.net.Server;
import org.martin.electroList.structure.ElectroList;

/**
 *
 * @author martin
 */
public class ConnectionManager extends Thread{
    private final String hostTest;
    private final long clientId;
    private final ElectroList<Boolean> listConnRight;
    private static final String ERROR = "Unreachable";
    
    public ConnectionManager(String hostTest, long clientId) {
        this.hostTest = hostTest;
        this.clientId = clientId;
        listConnRight = new ElectroList<>();
    }
    
    public boolean isConnected(){
        try {
            Process p = Runtime.getRuntime().exec("ping 192.168.0."+hostTest);
            p.waitFor(10, TimeUnit.MILLISECONDS);
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = reader.readLine();

            p.destroy();
            reader.close();
            return line.contains(ERROR);
        } catch (IOException ex) {
            Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    @Override
    public void run(){
        while (true) {            
            try {
                listConnRight.add(isConnected());
                Thread.sleep(3000);
                if (listConnRight.size() == 5) {
                    if (listConnRight.allMatch(cr->!cr)) {
                        Server.getServer().disconnectClient(clientId);
                        break;
                    }
                    listConnRight.clear();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
