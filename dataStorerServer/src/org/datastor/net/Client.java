/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.datastor.net;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.datastor.cmd.CmdOption;
import org.datastor.cmd.CmdOrder;
import org.datastor.cmd.Command;
import org.datastor.cmd.Interpreter;
import org.datastor.interfaces.Communicable;
import org.datastor.interfaces.InterpreterListener;
import org.datastor.model.User;
import org.datastor.streams.StringInputStream;
import org.datastor.streams.StringOutputStream;
import org.datastor.system.ServerMessages;
import static org.datastor.system.ServerMessages.HEADER_SEPARATOR;
import org.datastor.system.UserList;
import org.datastor.system.UserManager;
import org.dsEncryptor.exceptions.InvalidTextException;

/**
 *
 * @author martin
 */
public class Client implements Communicable{
    private final User user;
    private final UserManager userManager;
    private final Socket socket;
    private final StringOutputStream outputStream;
    private final StringInputStream inputStream;
    private final Interpreter interpreter;
    
    private boolean connected;
    
    private static final StringBuilder sBuild = new StringBuilder();
    
    // Cada cliente tendra un UserManager con el que se gestionara cada operacion con su usuario correspondiente
    // Algo smilar a lo que hacia con el CloudManager.
    
    
    public Client(User user, Socket socket) throws IOException {
        this(user, socket, new StringOutputStream(socket.getOutputStream()), 
                new StringInputStream(socket.getInputStream()));
    }

    public Client(User user, Socket socket, StringOutputStream buffWriter, StringInputStream buffReader) {
        this.user = user;
        this.userManager = new UserManager(user);
        this.socket = socket;
        this.outputStream = buffWriter;
        this.inputStream = buffReader;
        this.interpreter = new Interpreter(getInterListener());
        connected = true;
        //System.out.println(socket.getInetAddress().getHostAddress());
    }
    
    private InterpreterListener getInterListener(){
        return (Command cmd) -> {
            final String cmdOrder = cmd.getOrder();
            final int optCount = cmd.getOptionsCount();
            
            switch(cmdOrder){
                case CmdOrder.ADD:
                    if (optCount == 3 || optCount == 6) {
                        String data = cmd.getOptionAt(0);
                        boolean rightData = ((data.startsWith("'") && data.endsWith("'"))
                                || (data.startsWith("\"") && data.endsWith("\"")))
                                || cmd.hasNumberID(data);
                        System.out.println("RightData: "+rightData);
                        if (rightData) {
                            if (optCount == 3) {
                                boolean isTo = cmd.getOptionAt(1).equals(CmdOption.TO);
                                if (isTo) {
                                    String listName = cmd.getOptionAt(2);
                                    UserList list = userManager.getListBy(listName);
                                    if (list == null)
                                        buildMessage(ServerMessages.ERROR_OPTION,
                                                "La lista seleccionada no existe");
                                    else{
                                        list.add(data);
                                        buildMessage(ServerMessages.OK_MSG,
                                                "Dato agregado correctamente");
                                    }
                                }
                                else
                                    buildMessage(ServerMessages.ERROR_OPTION,
                                            "Opcion desconocida");
                            }
                            else{
                                boolean isIn = cmd.getOptionAt(1).equals(CmdOption.IN);
                                boolean isIndex = cmd.getOptionAt(2).equals(CmdOption.INDEX);
                                
                                if (isIn && isIndex) {
                                    int index;
                                    try {
                                        Long.parseLong(cmd.getOptionAt(3));
                                        index = Integer.parseInt(cmd.getOptionAt(3));
                                        
                                        if (index >= 0) {
                                            boolean isTo =
                                                    cmd.getOptionAt(4).equals(CmdOption.TO);
                                            
                                            if (isTo) {
                                                UserList list = userManager.getListBy(cmd.getOptionAt(5));
                                                if (list == null)
                                                    buildMessage(ServerMessages.GENERAL_ERROR,
                                                            "La lista seleccionada no existe");
                                                else{
                                                    list.add(index, data);
                                                    buildMessage(ServerMessages.OK_MSG,
                                                            "Dato agregado correctamente");
                                                }

                                            }
                                            else
                                                buildMessage(ServerMessages.ERROR_OPTION,
                                                        "Opcion incorrecta");
                                        }
                                        else
                                            buildMessage(ServerMessages.ERROR_OPTION,
                                                    "El indice debe ser igual o mayor a cero");
                                        
                                    } catch (NumberFormatException e) {
                                        buildMessage(ServerMessages.ERROR_OPTION, "El indice debe ser un valor numerico");
                                    }
                                }
                                else
                                    buildMessage(ServerMessages.ERROR_OPTION, "Opcion(es) incorrecta(s)");
                            }
                        }
                        else
                            buildMessage(ServerMessages.ERROR_OPTION, "Tipo de dato desconocido");
                    }
                    else
                        buildMessage(ServerMessages.ERROR_OPTION, "Cantidad de opciones invalida");
                    break;
                    
                case CmdOrder.CLOSE:
                    if (optCount == 0) {
                        buildMessage(ServerMessages.OK_MSG, "Conexion cerrada con exito");
                        connected = false;
                    }
                    else
                        buildMessage(ServerMessages.ERROR_OPTION, "Este comando no posee parametros");
                    break;
                    
                case CmdOrder.CLEAR:
                    if (optCount == 1 || optCount == 2) {
                        String firstOpt = cmd.getOptionAt(0);
                        switch (firstOpt) {
                            case CmdOption.ALL:
                                boolean rightCmd = optCount == 1 || (optCount == 2 && cmd.getOptionAt(1)
                                        .equals(CmdOption.LISTS));
                                if (rightCmd) {
                                    boolean clear = userManager.clearAll();
                                    if (clear)
                                        buildMessage(ServerMessages.OK_MSG, "Listas vaciadas correctamente");
                                    else
                                        buildMessage(ServerMessages.WARNING_ERROR, "Listas vacias");
                                }
                                else
                                    buildMessage(ServerMessages.ERROR_OPTION, "Opcion invalida: "+cmd.getOptionAt(1));
                                
                                break;
                            case CmdOption.LIST:
                                String listName = cmd.getOptionAt(1);
                                boolean cleared = userManager.clearAll(listName);
                                if (cleared)
                                    buildMessage(ServerMessages.OK_MSG, "Lista vaciada correctamente: "+listName);
                                else
                                    buildMessage(ServerMessages.GENERAL_ERROR, "La lista seleccionada no existe");
                                break;
                            default:
                                buildMessage(ServerMessages.ERROR_OPTION, "Opcion invalida: "+firstOpt);
                                break;
                        }
                    }
                    else
                        buildMessage(ServerMessages.ERROR_OPTION, "Cantidad de opciones invalida");
                    break;
                    
                case CmdOrder.CREATE:
                    if (optCount == 1)
                        if (userManager.create(cmd.getOptionAt(0)))
                            buildMessage(ServerMessages.OK_MSG, "Lista creada correctamente");
                        else
                            buildMessage(ServerMessages.GENERAL_ERROR, "Lista ya existe");
                    else
                        buildMessage(ServerMessages.ERROR_OPTION, "Cantidad de opciones invalida");
                    break;
                    
                case CmdOrder.DEL:
                    switch(optCount){
                        case 2:
                            final String firtsOp = cmd.getOptionAt(0);
                            if (firtsOp.equals(CmdOption.ALL) && cmd.getOptionAt(1).equals(CmdOption.LISTS)) {
                                userManager.deleteAll();
                                buildMessage(ServerMessages.OK_MSG, "Listas eliminadas correctamente");
                            }
                            else if (firtsOp.equals(CmdOption.LIST)) {
                                userManager.deleteList(cmd.getOptionAt(1));
                                buildMessage(ServerMessages.OK_MSG, "Lista eliminada "
                                        +cmd.getOptionAt(1)+" correctamente");
                            }
                            else
                                buildMessage(ServerMessages.ERROR_OPTION, "Una de las opciones es incrrecta");
                            break;
                            
                        case 4:
                            String firstOpt = cmd.getOptionAt(0);
                            String thirdOpt = cmd.getOptionAt(2);
                            boolean wrongFirst = !firstOpt.equals(CmdOption.INDEX);
                            boolean wrongThird = !thirdOpt.equals(CmdOption.FROM);
                            
                            if (!wrongFirst && !wrongThird) {
                                try {
                                    int index = Integer.parseInt(cmd.getOptionAt(1));
                                    boolean deleted = userManager.deleteInIndex(cmd.getOptionAt(3), index);
                                    if (deleted)
                                        buildMessage(ServerMessages.OK_MSG, "Dato eliminado con exito");
                                    else
                                        buildMessage(ServerMessages.GENERAL_ERROR, 
                                                "El dato o la lista a  que se intenta acceder no existe");
                                    
                                } catch (NumberFormatException e) {
                                    buildMessage(ServerMessages.ERROR_OPTION, 
                                            "El indice debe ser un valor numerico");
                                }
                            }
                            else{
                                buildMessage(ServerMessages.ERROR_OPTION, 
                                        wrongFirst && wrongThird ? "No se reconoce la opcion index y from"
                                                : (wrongFirst ? "No se reconoce opcion index" : 
                                                        "No se reconoce opcion from"));
                            }
                            
                            break;
                            
                        case 6:
                            String fOpt = cmd.getOptionAt(0);
                            String tOpt = cmd.getOptionAt(2);
                            boolean wrongF = !fOpt.equals(CmdOption.IN);
                            boolean wrongT = !tOpt.equals(CmdOption.WHERE);
                            boolean rightOpts = !wrongF && !wrongT;
                            if (rightOpts) {
                                final String operatorSymbol = cmd.getOptionAt(4);
                                final String listName = cmd.getOptionAt(1);
                                final String condition = cmd.getOptionAt(5);
                                
                                if (operatorSymbol.equals(CmdOption.EQUALS) 
                                        || operatorSymbol.equals(CmdOption.CONTAINS))
                                    if (userManager.deleteIf(listName, condition, operatorSymbol))
                                        buildMessage(ServerMessages.OK_MSG, "Eliminacion correcta");
                                    else
                                        buildMessage(ServerMessages.GENERAL_ERROR, "La lista seleccionada no existe");
                                else
                                    buildMessage(ServerMessages.ERROR_OPTION, "Se desconoce el operador");
                            }
                            else
                                buildMessage(ServerMessages.ERROR_OPTION, 
                                        wrongF && wrongT ? "No se reconoce la opcion in y where"
                                                : (wrongF ? "No se reconoce opcion in" : 
                                                        "No se reconoce opcion where"));
                            break;
                    }
                    break;
                    
                case CmdOrder.GET:
                    switch(optCount){
                        case 2:
                            String firstOp = cmd.getOptionAt(0);
                            String secondOp = cmd.getOptionAt(1);
                            
                            if (firstOp.equals(CmdOption.SIZEOF)) {
                                int sizeOf = 0;
                                if (secondOp.equals(CmdOption.ALL)) {
                                    sizeOf = userManager.getSizeOfAll();
                                    buildMessage(ServerMessages.GET_MSG, sizeOf);
                                }
                                else{
                                    sizeOf = userManager.getSizeOf(secondOp);
                                    if (sizeOf < 0)
                                        buildMessage(ServerMessages.GENERAL_ERROR, "La lista seleccionada no existe");
                                    else
                                        buildMessage(ServerMessages.GET_MSG, sizeOf);
                                }
                            }
                            else
                                buildMessage(ServerMessages.ERROR_OPTION, "La opcion "
                                        +firstOp+" es invalida");
                            break;
                            
                        case 3:
                            String firstO = cmd.getOptionAt(0);
                            int index = -2;
                            
                            if (firstO.equals(CmdOption.ALL)
                                    && cmd.getOptionAt(1).equals(CmdOption.FROM)) {
                                String elements = userManager.getAllFrom(cmd.getOptionAt(2));
                                if (elements.equals(ServerMessages.NULL))
                                    buildMessage(ServerMessages.GENERAL_ERROR, "La lista seleccionada no existe");
                                else
                                    buildMessage(ServerMessages.GET_MSG, elements);
                            }
                            else if ((index = cmd.getOptionAsNumber(0)) > -1) {
                                String element = userManager.getByIndexFrom(cmd.getOptionAt(2), index);
                                switch(element){
                                    case ServerMessages.BAD_RANGE:
                                        buildMessage(ServerMessages.BAD_RANGE,
                                                "El indice ingresado no es valido");
                                        break;
                                    
                                    case ServerMessages.NULL:
                                        buildMessage(ServerMessages.ERROR_ORDER, "La lista seleccionada no existe");
                                        break;
                                    
                                    default:
                                        buildMessage(ServerMessages.GET_MSG, element);
                                        break;
                                }
                            }
                            else
                                buildMessage(ServerMessages.ERROR_OPTION, "La(s) opcion(es) "
                                        + "ingresada(s) no es(son) valida(s)");
                            break;
                            
                        case 4:
                            boolean rightOp1 = false;
                            try {
                                int start = Integer.parseInt(cmd.getOptionAt(0));
                                rightOp1 = true;
                                int end = Integer.parseInt(cmd.getOptionAt(1));
                                
                                if (cmd.getOptionAt(2).equals(CmdOption.FROM)) {
                                    final String listName = cmd.getOptionAt(3);
                                    String results = userManager.getByRangeFrom(listName, start, end);
                                    switch(results){
                                        case ServerMessages.BAD_RANGE:
                                            buildMessage(ServerMessages.BAD_RANGE, 
                                                    "El(los) indice(s) no es(son) valido(s)");
                                            break;
                                            
                                        case ServerMessages.NULL:
                                            buildMessage(ServerMessages.ERROR_ORDER, "La lista seleccionada no existe");
                                            break;
                                            
                                        default:
                                            buildMessage(ServerMessages.OK_MSG, results);
                                            break;
                                    }
                                    
                                }
                                else
                                    buildMessage(ServerMessages.ERROR_OPTION, "No se reconoce opcion from");
                                
                            } catch (NumberFormatException e) {
                                if (!rightOp1) {
                                    try {
                                        Integer.parseInt(cmd.getOptionAt(1));
                                        buildMessage(ServerMessages.ERROR_OPTION, 
                                                "La opcion 1 no es un valor entero");
                                    } catch (NumberFormatException e2) {
                                        buildMessage(ServerMessages.ERROR_OPTION, 
                                                "La opcion 1 y 2 no son un valor entero");
                                    }
                                }
                                else
                                    buildMessage(ServerMessages.ERROR_OPTION, 
                                            "La opcion 2 no es un valor entero");
                            }
                            break;
                            
                        case 5:
                            String firstOpt = cmd.getOptionAt(0);
                            switch(firstOpt){
                                case CmdOption.TYPEOF:
                                    
                                    break;
                                    
                                case CmdOption.COUNT:
                                    
                                    break;
                                    
                                default:
                                    buildMessage(ServerMessages.ERROR_OPTION, "La debe especificar una opcion typeof o count");
                                    break;
                            }
                            
                            break;
                            
                        case 7:
                            
                            break;
                            
                        default:
                            buildMessage(ServerMessages.ERROR_OPTION, "Numero de opciones invalido");
                            break;
                    }
                    break;
                    
                case CmdOrder.HELP:
                    break;
                    
                case CmdOrder.RESTORE:
                    
                    break;
                    
                case CmdOrder.SET:
                    
                    break;
                    
                default:
                    buildMessage(ServerMessages.ERROR_ORDER, "Orden no valida");
                    break;
            }
        };
    }
    
    private void buildMessage(String header, Object msg){
        StringBuilder sbMsg = new StringBuilder();
        sbMsg.append(header).append(HEADER_SEPARATOR).append(msg.toString())
                .append(ServerMessages.RESPONSE_MSG);
        interpreter.setResponseMsg(sbMsg.toString());
        System.out.println("Client: InterpreterMessage: "+interpreter.getResponseMsg());
    }

    public boolean isConnected() {
        return connected;
    }

    public String getRemoteAddress(){
        return socket.getInetAddress().getHostAddress();
    }
    
    public void closeStreams(){
        try {
            socket.shutdownOutput();
            socket.shutdownInput();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeSocket(){
        try {
            socket.close();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeConnection(){
        closeStreams();
        closeSocket();
    }

    public User getUser() {
        return user;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public Interpreter getInterpreter() {
        return interpreter;
    }

    @Override
    public void sendData(String data) {
        try {
            System.out.println("Texto en sendData: "+data);
            outputStream.writeString(data);
        } catch (IOException | InvalidTextException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getReceivData() {
        try {
            return inputStream.readString();
        } catch (IOException | InvalidTextException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}






